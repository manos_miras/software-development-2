﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleProj
{
    public class Example
    {
        public int add(int x, int y, int z)
        {
            return x+y+z;
        }

        public double div(double x, double y)
        {
            return x / y;
        }

        public double percent(double total, double amount)
        {
            return (total * amount) / 100;
        }

        public double VAT(int pence)
        {
            return Convert.ToDouble(pence) * 0.2;
        }

        public double addVAT(int pence)
        {
            return Convert.ToDouble(pence) + VAT(pence);
        }

        public double dist(int x1,int y1,int x2,int y2){
            return Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
        }

        public double circumference(int rad)
        {
            return 2 * 3.14 * rad;
        }
    }
}
