﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExampleProj;

namespace TestExampleProj
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void addTest()
        {
            int Parameter1 = 3;
            int Parameter2 = 4;
            int Parameter3 = 5;
            int expectedResult = 12;
            Example example = new Example();

            //act

            int actual = example.add(Parameter1, Parameter2, Parameter3);

            //assert
            Assert.AreEqual(actual, expectedResult);
        }

        [TestMethod]
        public void divTest()
        {
            double Parameter1 = 7;
            double Parameter2 = 2;
            double expectedResult = 3.5;
            Example example = new Example();

            //act
            double actual = example.div(Parameter1, Parameter2);

            //assert
            Assert.AreEqual(actual, expectedResult);
        }

        [TestMethod]
        public void percentTest()
        {
            double total = 245;
            double amount = 30;
            double expectedResult = 73.5;
            Example example = new Example();

            //act
            double actual = example.percent(total, amount);

            //assert
            Assert.AreEqual(actual, expectedResult);
        }

        [TestMethod]
        public void VATTest()
        {
            int pence = 30;
            double expectedResult = 6;
            Example example = new Example();

            //act
            double actual = example.VAT(pence);

            //assert
            Assert.AreEqual(actual, expectedResult);
        }

        [TestMethod]
        public void addVATTest()
        {
            int pence = 30;
            double expectedResult = 36;
            Example example = new Example();

            //act
            double actual = example.addVAT(pence);

            //assert
            Assert.AreEqual(actual, expectedResult);
        }

        [TestMethod]
        public void distTest()
        {
            int x1 = 5;
            int y1 = 6;
            int x2 = -7;
            int y2 = 11;
            double expectedResult = 13;
            Example example = new Example();

            //act
            double actual = example.dist(x1, y1, x2, y2);

            //assert
            Assert.AreEqual(actual, expectedResult);
        }

        [TestMethod]
        public void circumferenceTest()
        {
            int rad = 2;
            double expectedResult = 12.56;
            Example example = new Example();

            //act
            double actual = example.circumference(rad);



            //assert
            Assert.AreEqual(actual, expectedResult);
        }
    }
}
