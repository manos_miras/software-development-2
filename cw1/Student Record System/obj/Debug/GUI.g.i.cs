﻿#pragma checksum "..\..\GUI.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "5FE2167919D4FCA511367840817448DA"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Student_Record_System;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Student_Record_System {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblName;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblDateOfBirth;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCourse;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblMatricNumber;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblLevel;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCredits;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBoxFirstName;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBoxDateOfBirth;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBoxCourse;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBoxMatricNumber;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBoxLevel;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBoxCredits;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBoxSecondName;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSet;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClear;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGet;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAward;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAdvance;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblDateIndicator;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Student Record System;component/gui.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\GUI.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.lblName = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.lblDateOfBirth = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.lblCourse = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.lblMatricNumber = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.lblLevel = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.lblCredits = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.txtBoxFirstName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.txtBoxDateOfBirth = ((System.Windows.Controls.TextBox)(target));
            
            #line 17 "..\..\GUI.xaml"
            this.txtBoxDateOfBirth.GotFocus += new System.Windows.RoutedEventHandler(this.txtBoxDateOfBirth_GotFocus);
            
            #line default
            #line hidden
            return;
            case 9:
            this.txtBoxCourse = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.txtBoxMatricNumber = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.txtBoxLevel = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.txtBoxCredits = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.txtBoxSecondName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.btnSet = ((System.Windows.Controls.Button)(target));
            
            #line 23 "..\..\GUI.xaml"
            this.btnSet.Click += new System.Windows.RoutedEventHandler(this.btnSet_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.btnClear = ((System.Windows.Controls.Button)(target));
            
            #line 24 "..\..\GUI.xaml"
            this.btnClear.Click += new System.Windows.RoutedEventHandler(this.btnClear_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.btnGet = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\GUI.xaml"
            this.btnGet.Click += new System.Windows.RoutedEventHandler(this.btnGet_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.btnAward = ((System.Windows.Controls.Button)(target));
            return;
            case 18:
            this.btnAdvance = ((System.Windows.Controls.Button)(target));
            
            #line 27 "..\..\GUI.xaml"
            this.btnAdvance.Click += new System.Windows.RoutedEventHandler(this.btnAdvance_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.lblDateIndicator = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

