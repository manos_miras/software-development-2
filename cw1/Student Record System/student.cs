﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Student_Record_System
{
    class student
    {
        #region properties

        // Stores matriculation number of student (In the range 40000 to 60000)
        private ushort matriculationNumber;

        // Stores first name of the student
        private string firstName;

        // Stores second name of the student
        private string secondName;

        // Stores the date of birth of student
        private DateTime dateOfBirth;

        // Stores the course of the student
        private string course;

        // Stores the level of the student (In the range 1-4)
        private ushort level;

        // Stores student credits (In the range 0 to 480)
        private ushort credits;

        #endregion

        #region accessors

        // Gets or sets the value of matriculationNumber
        public ushort MatriculationNumber 
        {
            get { return matriculationNumber; }
            set
            {
                // If the value entered is not between the range of 40000 to 60000, an error is shown
                if (value < 40000 || value > 60000)
                {
                    throw new ArgumentException("Invalid matriculation number.");
                }
                matriculationNumber = value;

            }
        }

        // Gets or sets the value of firstName
        public string FirstName
        {
            get { return firstName; }
            
            set
            {
                // If the value entered is blank, an error is shown
                if (value == "")
                {
                    throw new ArgumentException("First name can not be blank.");
                }
                firstName = value;
            }
        }

        // Gets or sets the value of secondName
        public string SecondName
        {
            get { return secondName; }
            set
            {
                // If the value entered is blank, an error is shown
                if (value == "")
                {
                    throw new ArgumentException("Second name can not be blank.");
                }
                secondName = value;
            }
        }

        // Gets or sets the value of dateOfBirth
        public DateTime DateOfBirth
        {
            get { return dateOfBirth; }
            set
            {
                // If the value entered is blank, an error is shown
                if (value == null)
                {
                    throw new ArgumentException("Date of birth can not be blank.");
                }
                dateOfBirth = value;
            }
        }

        // Gets or sets the value of course
        public string Course
        {
            get { return course; }
            set
            {
                // If the value entered is blank, an error is shown
                if (value == "")
                {
                    throw new ArgumentException("Course can not be blank.");
                }
                course = value;
            }
        }

        // Gets or sets the value of level
        public ushort Level
        {
            get { return level; }
            set
            {
                if (value < 1 || value > 4)
                {
                    throw new ArgumentException("Invalid student level.");
                }
                level = value;
            }
        }

        // Gets or sets the value of credits
        public ushort Credits
        {
            get { return credits; }
            set
            {
                if (value < 0 || value > 480)
                {
                    throw new ArgumentException("Invalid amount of credits.");
                }
                credits = value;
            }
        }

        #endregion

        #region methods
        // Advances the student by 1 level
        public void advance(student s)
        {
            switch (s.level)
            {
                case 1:
                    if (s.credits >= 120)
                        s.level = 2;
                    else
                        MessageBox.Show("Insufficient credits, student may not advance.");
                    break;
                case 2:
                    if (s.credits >= 240)
                        s.level = 3;
                    else
                        MessageBox.Show("Insufficient credits, student may not advance.");
                    break;
                case 3:
                    if (s.credits >= 360)
                        s.level = 4;
                    else
                        MessageBox.Show("Insufficient credits, student may not advance.");
                    break;
                case 4:
                    MessageBox.Show("Student is already in 4th year");
                    break;
            }
        }

        // Will return a message displaying the award to be conferred to a student.
        public void award(student s)
        {
            if (s.credits >= 0 && s.credits <= 359)
                MessageBox.Show("Certificate of Higher Education");
            else if (s.credits >= 360 && s.credits <= 479)
                MessageBox.Show("Degree");
            else
                MessageBox.Show("Honours degree");

        }
        #endregion
    }
}
