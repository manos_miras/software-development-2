﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Student_Record_System
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // Creates a new student called s
        student s = new student();

        //Clears all the values from the textboxes.
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtBoxFirstName.Clear();
            txtBoxSecondName.Clear();
            txtBoxDateOfBirth.Clear();
            txtBoxCourse.Clear();
            txtBoxMatricNumber.Clear();
            txtBoxLevel.Clear();
            txtBoxCredits.Clear();
        }

        // Sets values entered in textboxes to relevant student properties.
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            try
            { 
                // Assign student first name
                s.FirstName = txtBoxFirstName.Text;
                // Assign student second name
                s.SecondName = txtBoxSecondName.Text;

                // Date of birth field validation.
                DateTime validDateTime;
                bool succeeded = DateTime.TryParse(txtBoxDateOfBirth.Text, out validDateTime);
                s.DateOfBirth = validDateTime;

                // If the date of birth field has invalid characters, an error is shown.
                if (!succeeded)
                {
                    throw new ArgumentException("Please enter a valid date into the Date of Birth text box");
                }
                s.Course = txtBoxCourse.Text;
                if (isNumeric(txtBoxMatricNumber.Text))
                    s.MatriculationNumber = ushort.Parse(txtBoxMatricNumber.Text);
                else
                    throw new ArgumentException("Please enter a number between 40000 and 60000 into the Matriculation Number text box");
                if (isNumeric(txtBoxLevel.Text))
                    s.Level = ushort.Parse(txtBoxLevel.Text);
                else
                    throw new ArgumentException("Please enter a number between 1 and 4 into the Level text box");
                if (isNumeric(txtBoxCredits.Text))
                    s.Credits = ushort.Parse(txtBoxCredits.Text);
                else
                    throw new ArgumentException("Please enter a number between 0 and 480 into the Credits text box");

            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        // Retrieves all of the student information to the textboxes.
        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            txtBoxFirstName.Text = s.FirstName;
            txtBoxSecondName.Text = s.SecondName;
            txtBoxDateOfBirth.Text = s.DateOfBirth.ToString("dd/MM/yyyy"); ;
            txtBoxCourse.Text = s.Course;
            txtBoxMatricNumber.Text = s.MatriculationNumber.ToString();
            txtBoxLevel.Text = s.Level.ToString();
            txtBoxCredits.Text = s.Credits.ToString();
        }

        // Advances the student's level by calling advance()
        private void btnAdvance_Click(object sender, RoutedEventArgs e)
        {
            s.advance(s);
            txtBoxLevel.Text = s.Level.ToString();
        }

        // Clears the prompt text from the DateOfBirth textbox once user has clicked on it for the first time
        public void txtBoxDateOfBirth_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= txtBoxDateOfBirth_GotFocus;
        }

        //Checks if a value is numeric or not
        private static Boolean isNumeric(string input)
        {
            //Int output: Used to hold in the output from TryParse
            ushort output;
            //Checks if entered value is numeric or not
            if (ushort.TryParse(input, out output) == false)
                return false;
            else
                return true;
        }
    }
}
