﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Universal_Installer
{
    /// <summary>
    /// Interaction logic for WindowEnd.xaml
    /// </summary>
    public partial class WindowEnd : Window
    {
        public WindowEnd(string name, string location)
        {
            InitializeComponent();
            labelEnd.Content = "Your name is: " + name + " and you are located at: " + location;
        }

        private void btnFinished_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
