﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Universal_Installer
{
    /// <summary>
    /// Interaction logic for WindowLocation.xaml
    /// </summary>
    public partial class WindowLocation : Window
    {
        string theName;
        public WindowLocation(string name)
        {
            InitializeComponent();
            theName = name;
        }

        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            Close();
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            string location = listBox.SelectedItems[0].ToString();
            WindowEnd endWindow = new WindowEnd(theName, location);
            endWindow.Show();
            Close();
        }
    }
}
