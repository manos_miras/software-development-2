﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace Database_Tutorial
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            label1.Content = "Select a Country";

            SqlConnection con =
            new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\SET08108T9.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            SqlCommand com = new SqlCommand(
                "SELECT name FROM bbc", con);
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                worldCbx.Items.Add(sdr["name"]);
            }
            sdr.Close();


        }

        private void worldCbx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String selectedCountry = worldCbx.SelectedValue.ToString();
            //MessageBox.Show(selectedCountry);

            CountryFrm cf = new CountryFrm();
            cf.Show();
            cf.setCountry(selectedCountry);
        }

    }
}
