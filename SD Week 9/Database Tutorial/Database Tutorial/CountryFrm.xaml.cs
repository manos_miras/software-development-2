﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace Database_Tutorial
{
    /// <summary>
    /// Interaction logic for CountryFrm.xaml
    /// </summary>
    public partial class CountryFrm : Window
    {
        public CountryFrm()
        {
            InitializeComponent();
        }

        public void setCountry(String name)
        {
            SqlConnection con =
        new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\SET08108T9.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            String sql = String.Format(
                @"SELECT name,region,area,population,gdp
          FROM bbc WHERE name='{0}'",
                name);
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                txtBox_Name.Text = (String)sdr["name"];
                txtBox_Region.Text = (String)sdr["region"];
                txtBox_Area.Text = (String)sdr["area"].ToString();
                txtBox_Population.Text = sdr["population"].ToString();
                txtBox_GDP.Text = sdr["gdp"].ToString();
            }
            sdr.Close();

        }
    }
}
