﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Prime_Numbers
{
    class Program
    {
        //Checks if a number is prime
        private static int CheckPrime(int number)
        {
            //Integer used for accumulation
            int i;
            //Go through every number from 2 up to number - 1
            for (i = 2; i <= number - 1; i++)
            {
                //If the quotient of the division of every number before our number is = 0 then our number is not a prime number
                if (number % i == 0)
                {
                    return 0;
                }
            }
            //If i is equal to number then the number is prime
            if (i == number)
            {
                return 1;
            }
            //Else not
            return 0;
        }

        //Checks if a value is numeric or not
        private static Boolean isNumeric(string input)
        {
            //Int output: Used to hold in the output from TryParse
            int output;
            //Checks if entered value is numeric or not
            if (int.TryParse(input, out output) == false)
                return false;
            else
                return true;
        }

        static void Main(string[] args)
        {
            string input = "1";
            //Asks for a number between a given range and loops until it is given a correct number.
            while (true)
            { 
                //Loops until user has entered numeric value
                while (true)
                {
                    //Prompts user to enter number
                    Console.WriteLine("Enter a number between 1 and 10,000");
                    //Saves the number
                    input = Console.ReadLine();
                    //Uses isNumeric to check if the value enterd is a number
                    if (isNumeric(input) == false)
                        Console.WriteLine("You have entered a non numeric value, please try again.");
                    else
                        break;
                }
                //If the number is between given range then exit loop
                if (Int32.Parse(input) <= 10000 && Int32.Parse(input) >= 1)
                    break;
            }
            //Print every number that is prime up to the number that the user specified
            for (int i = 1; i <= Int32.Parse(input); i++)
            { 
                if (CheckPrime(i) == 1)
                    Console.WriteLine(i);
            }

            //Force the user to hit return, otherwise the app just exits without giving the // user chance to see what's going on.
            Console.WriteLine("Press any key to exit");
            Console.ReadLine();
        }
    }
}
