﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculate_House_Area
{
    class Program
    {
        // Takes in a string and returns an int
        public static int StringToInt(string text)
        {
            return (Int32.Parse(text));
        }

        // Calculates the area by multiplying width and length
        public static int CalculateArea(int width, int length)
        {
            return (width * length);
        }

        //Checks if a value is numeric or not
        private static Boolean isNumeric(string input)
        {
            //Int output: Used to hold in the output from TryParse
            int output;
            //Checks if entered value is numeric or not
            if (int.TryParse(input, out output) == false)
                return false;
            else
                return true;
        }

        static void Main(string[] args)
        {
            string roomWidth;
            string roomLength;
            string[] roomName = new string[100];
            int[] roomSize = new int[100];
            //Used to sum total room area
            int roomSum = 0;
            //Used to recognise which room is being calculated
            int arrayCount = 0;
            //Asks the user to input room name, room width and room length.
            while (roomName[arrayCount] != "end")
            {
                Console.WriteLine("Please enter room name (Type end if no more rooms.)");
                roomName[arrayCount] = Console.ReadLine();
                //Exits if user typed 'end' when prompted for room name
                if (roomName[arrayCount] == "end")
                {
                    //arrayCount-- is used in order for 'end' not to show up on final output
                    arrayCount--;
                    break;
                }
                while (true)
                {
                    //Prompts the user to enter the room width
                    Console.WriteLine("Please enter room width:");
                    //Saves user input
                    roomWidth = Console.ReadLine();
                    //Uses isNumeric to check if the value enterd is a number
                    if (isNumeric(roomWidth) == false)
                        Console.WriteLine("You have entered a non numeric value, please try again.");
                    else
                        break;
                }
                //Cast from string to int
                int width = StringToInt(roomWidth);
                while (true)
                {
                    //Prompts the user to enter the room length
                    Console.WriteLine("Please enter room length:");
                    //Saves user input
                    roomLength = Console.ReadLine();
                    //Uses isNumeric to check if the value enterd is a number
                    if (isNumeric(roomLength) == false)
                        Console.WriteLine("You have entered a non numeric value, please try again.");
                    else
                        break;
                }
                //Cast from string to int
                int length = StringToInt(roomLength);
                //Calculates the area for this room
                roomSize[arrayCount] = CalculateArea(width, length);
                //Itterate room counter
                arrayCount++;
                //Total room area
                roomSum = roomSum + CalculateArea(width, length);
            }
            //Outputs and aligns text
            Console.WriteLine(String.Format("{0,-10}  {1,10}", "Room", "Size(sq ms)"));
            //Outputs and aligns seperate room results
            for (int i = 0; i <= arrayCount; i++)
            {
                Console.WriteLine(String.Format("{0,-10}  {1,10}", roomName[i], roomSize[i]));
            }
            //Outputs and aligns final results
            Console.WriteLine(String.Format("{0,-10}  {1,10}", "Total", roomSum));



            //Force the user to hit return, otherwise the app just exits without giving the // user chance to see what's going on.
            Console.WriteLine("Press any key to exit");
            Console.ReadLine();

        }
    }
}
