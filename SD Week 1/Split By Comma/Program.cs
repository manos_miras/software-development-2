﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Split_By_Comma
{
    class Program
    {
        static void Main(string[] args)
        {
            //Sets the char that will be the seperator
            char[] delimiterChar = { ',' };
            //Numbers is used to calculate the sum
            int numbers = 0;

            Console.WriteLine("Enter your set of numbers, seperated by commas (For instance: 1,2,3,4,5)");
            string text = Console.ReadLine();
            //Seperates the given string into seperate array instances
            string[] result = text.Split(delimiterChar);
            //Adds up every instance of the array to get a Sum
            foreach (string s in result)
                numbers = numbers + Int32.Parse(s);
            //Displays the sum
            Console.WriteLine("Result: " + numbers);

            //Force the user to hit return, otherwise the app just exits without giving the // user chance to see what's going on.
            Console.WriteLine("Press any key to exit");
            Console.ReadLine();

        }
    }
}
