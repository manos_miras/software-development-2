﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
namespace UniRecords
{
    class University
    {
        // List of students
        private List<Student> _students = new List<Student>();

        // List of lectures
        private List<Lecture> _lectures = new List<Lecture>();

        // // List of students accessor, only get.
        public List<Student> Students
        {
            get { return _students; }
        }

        // List of lectures accessor, only get.
        public List<Lecture> Lectures
        {
            get { return _lectures; }
        }

        // Adds a student to the student list.
        public void addStudent(Student aStudent)
        {
            _students.Add(aStudent);
        }

        public void showStudents()
        {
            string output = "";

            foreach (Student currentStudent in _students)
            {
                output += "Name: " + currentStudent.Name + ", Matric no: " + currentStudent.MatricNo + "\n";
            }

            MessageBox.Show(output);
        }

        public void addLecture(Lecture aLecture)
        {
            _lectures.Add(aLecture);
        }

        public void showLectures()
        {
            string output = "";

            foreach (Lecture currentLecture in _lectures)
            {
                output += "Title: " + currentLecture.Title + ", Tutor: " + currentLecture.TutorName + "\n";
            }

            MessageBox.Show(output);
        }

        public void enroll()
        {
            Lecture tempLecture = new Lecture();
            foreach (Student currentStudent in _students)
            {
                currentStudent.Lecture = tempLecture.Title;
            }
        }
    }
}
