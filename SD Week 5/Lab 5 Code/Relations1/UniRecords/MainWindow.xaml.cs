﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UniRecords
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // Creates a new university object.
        University university = new University();


        // Event Handler for btn_AddStudent, gets student info by retrieving from textboxes
        private void btn_AddStudent_Click(object sender, RoutedEventArgs e)
        {
            Student student = new Student();
            student.Name = txtBox_studentName.Text;
            student.MatricNo = txtBox_matricNo.Text;
            student.DateOfBirth = txtBox_dateOfBirth.Text;
            student.Address = txtBox_address.Text;
            university.addStudent(student);
            
        }

        // Event Handler for btn_ShowStudents, Shows students in a messagebox.
        private void btn_showStudents_Click(object sender, RoutedEventArgs e)
        {
            university.showStudents();
        }

        // Event Handler for btn_ClassListStudents, adds student details to the list box
        private void btn_ClassListStudents_Click(object sender, RoutedEventArgs e)
        {
            foreach (Student currentstudent in university.Students)
            {
                LstBox_studentList.Items.Add("Name: " + currentstudent.Name + ", Matric: " + currentstudent.MatricNo
                    + ", Date of Birth: " + currentstudent.DateOfBirth + ", Address: " + currentstudent.Address + ", Lecture: " + currentstudent.Lecture);
            }
        }

        // Event Handler for btn_clear, clears the list box.
        private void btn_clear_Click(object sender, RoutedEventArgs e)
        {
            LstBox_studentList.Items.Clear();
        }

        private void btn_addLecture_Click(object sender, RoutedEventArgs e)
        {
            Lecture lecture = new Lecture();
            lecture.Title = txtBox_lectureTitle.Text;
            lecture.TutorName = txtBox_lectureTutor.Text;
            university.addLecture(lecture);
        }

        private void btn_showLectures_Click(object sender, RoutedEventArgs e)
        {
            university.showLectures();
        }

        private void btn_lectureList_Click(object sender, RoutedEventArgs e)
        {
            foreach (Lecture currentlecture in university.Lectures)
            {
                LstBox_lectureList.Items.Add("Title: " + currentlecture.Title + ", Tutor: " + currentlecture.TutorName);
            }
        }

        private void btn_lectureClear_Click(object sender, RoutedEventArgs e)
        {
            LstBox_lectureList.Items.Clear();
        }

        private void btn_Enroll_Click(object sender, RoutedEventArgs e)
        {
            university.enroll();
        }

    }
}
