﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniRecords
{
    public class Lecture
    {
        private string _title = "SD1";
        private string _tutorName = "Andrew Cumming";

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string TutorName
        {
            get { return _tutorName; }
            set { _tutorName = value; }
        }


    }
}
