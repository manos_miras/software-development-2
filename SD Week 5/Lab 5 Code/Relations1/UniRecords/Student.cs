﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniRecords
{
    public class Student
    {
        private string _name;
        private string _address;
        string _dateOfBirth;
        private string _matricNo;
        private string _lecture = "none";


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string DateOfBirth
        {
            get { return _dateOfBirth; }
            set { _dateOfBirth = value; }
        }

        public string MatricNo
        {
            get { return _matricNo; }
            set { _matricNo = value; ; }
        }

        public string Lecture
        {
            get { return _lecture; }
            set { _lecture = value; }
        }


    }
}
