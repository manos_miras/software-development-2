﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Relations1
{
    class House
    {
        
        private List<Person> people = new List<Person>();
        
        public void addPerson(Person aPerson)
        {
            people.Add(aPerson);
        }

        public void printPeople()
        {
            foreach (Person currentperson in people)
            {
                Console.WriteLine(currentperson.name);
            }
        }
    }
}
