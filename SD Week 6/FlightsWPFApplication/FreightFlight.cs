﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FlightsWPFApplication
{
    public class FreightFlight : Flight
    {
        private double _weight;
        private string _airline;
        private string _destination;
        private string _departureTime;
        public string Airline
        {
            get { return _airline; }
            set { _airline = value; }
        }

        public string Destination
        {
            get { return _destination; }
            set { _destination = value; }
        }

        public string DepartureTime
        {
            get { return _departureTime; }
            set { _departureTime = value; }
        }

        public double Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        public void Show()
        {
            MessageBox.Show("Type: Freight Flight \nAirline: " + this.Airline + ", Destination: " + this.Destination
                + ", Departure Time: " + this.DepartureTime + ", Weight: " + this.Weight);
        }

        public FreightFlight(string airline, string destination, string departureTime, double weight)
        {
            Airline = airline;
            Destination = destination;
            DepartureTime = departureTime;
            Weight = weight;
        }
    }
}
