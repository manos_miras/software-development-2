﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FlightsWPFApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            cboBox_FlightType.SelectedItem = cbi1;
        }

        List<Flight> flights = new List<Flight>();


        private void cboBox_FlightType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cboBox_FlightType.SelectedItem == cbi1)
            {
                lbl_PassCountOrWeight.Content = "Passenger Count";
            }
            else
                lbl_PassCountOrWeight.Content = "Weight";
        }

        private void btn_Add_Click(object sender, RoutedEventArgs e)
        {
            if (cboBox_FlightType.SelectedItem == cbi1)
            {
                PassengerFlight passFlight = new PassengerFlight(txtBox_Airline.Text, txtBox_DepartureTime.Text, txtBox_Destination.Text, int.Parse(txtBox_PassCountOrWeight.Text));

                flights.Add(passFlight);

            }
            else
            {
                FreightFlight freightFlight = new FreightFlight(txtBox_Airline.Text, txtBox_DepartureTime.Text, txtBox_Destination.Text, double.Parse(txtBox_PassCountOrWeight.Text));

                flights.Add(freightFlight);
            }
        }

        private void btn_Show_Click(object sender, RoutedEventArgs e)
        {
            foreach (Flight currentFlight in flights)
            {
                currentFlight.Show();
            }
        }


        
    }
}
