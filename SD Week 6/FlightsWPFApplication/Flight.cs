﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightsWPFApplication
{
    public interface Flight
    {

        string Airline { get; set; }
        string Destination { get; set; }
        string DepartureTime { get; set; }

        

        #region methods

        void Show();

        #endregion methods

    }
}
