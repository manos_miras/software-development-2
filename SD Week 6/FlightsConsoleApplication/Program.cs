﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightsWPFApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Flight> flights = new List<Flight>();

            FreightFlight flight1 = new FreightFlight();
            flight1.Airline = "Fly by";
            flight1.DepartureTime = "10:00";
            flight1.Destination = "Athens";
            flight1.Weight = 12.32;

            flights.Add(flight1);

            PassengerFlight flight2 = new PassengerFlight();

            flight2.Airline = "MediumJet";
            flight2.DepartureTime = "13:00";
            flight2.Destination = "Budapest";
            flight2.PassengerCount = 90;

            flights.Add(flight2);

            foreach (Flight currentflight in flights)
            {
                Console.WriteLine("Airline: " + currentflight.Airline + ", Destination: " + currentflight.Destination
                + ", Departure Time: " + currentflight.DepartureTime);
            }

            Console.ReadLine();


        }
    }
}
