﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightsWPFApplication
{
    public class PassengerFlight : Flight
    {
        private int _passengerCount;
        private string _airline;
        private string _destination;
        private string _departureTime;
        public string Airline
        {
            get { return _airline; }
            set { _airline = value; }
        }

        public string Destination
        {
            get { return _destination; }
            set { _destination = value; }
        }

        public string DepartureTime
        {
            get { return _departureTime; }
            set { _departureTime = value; }
        }

        public int PassengerCount
        {
            get { return _passengerCount; }
            set { _passengerCount = value; }
        }

        public void Print()
        { 
            System.Console.WriteLine("Type: Passenger Flight /n Airline: " + this.Airline + ", Destination: " + this.Destination 
                + ", Departure Time: " + this.DepartureTime + ", Passenger Count: " + this.PassengerCount);
        }

    }
}
