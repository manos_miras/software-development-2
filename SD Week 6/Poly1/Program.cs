﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Poly1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Vehicle> vehicles = new List<Vehicle>();

            // Creates a vehicle object.
            Vehicle vehicle1 = new Vehicle();

            // Assign vehicle properties.
            vehicle1.make = "BMW";
            vehicle1.registration = "AAA333";

            // Adds vehicle object to vehicles list.
            vehicles.Add(vehicle1);

            // Creates a truck object.
            Truck truck1 = new Truck();

            // Assign truck properties.
            truck1.make = "Astra";
            truck1.registration = "BBB232";
            truck1.weight = 666;

            // Adds truck object to vehicles list.
            vehicles.Add(truck1);


            // Creates a car object.
            Car car1 = new Car();

            // Assign car properties.
            car1.make = "Baron";
            car1.registration = "CCC432";
            car1.seats = 5;

            // Adds car object to vehicles list.
            vehicles.Add(car1);


            vehicle1.print();

            truck1.print();

            car1.print();




            Console.ReadLine();
        }
    }
}
