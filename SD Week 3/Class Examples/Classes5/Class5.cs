using System;

namespace Classes5
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{

			// DONE Put code here to create 3 car instances

            AnotherCar car1 = new AnotherCar();// contructor method with no arguments passed
            car1.Colour = "Red";
            car1.Make = "Ford";
            car1.Type = "Mondeo";
            car1.Cost = 15000;
            

            AnotherCar car2 = new AnotherCar("Vauxhall");// contructor method with one argument passed
            car2.Colour = "Blue";
            car2.Type = "Astra";
            car2.Cost = 20000;
            

            AnotherCar car3 = new AnotherCar("FancyCarMake", "FancyCarType");
            car3.Colour = "Black";
            car3.Cost = 50000;
            


			// DONE Add a method named 'Display'to the AnotherCar Class
			// DONE This method should output the details of a  car instance
			// DONE to the Console

			// DONE Put code here to call that method to display the 
			// DONE details of a each car instance 
            car1.Display(car1);
            car2.Display(car2);
            car3.Display(car3);
		}
	}
}
