using System;
using System.Collections;

namespace Classes10
{
	/// <summary>
	/// Summary description for Rainbow.
	/// </summary>
	public class PaintShop
	{
		public ArrayList colourList = new ArrayList();
		public PaintShop()
		{
			
		}
		// add a display method that displays all the colours
		// in a PaintShop
        public void Display()
        {
            foreach (Colour2 c in colourList)
            {
                System.Console.WriteLine("{0}", c.Name);
            }
        }
	}
}
