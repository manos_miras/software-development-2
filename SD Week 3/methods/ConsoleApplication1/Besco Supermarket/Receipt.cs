﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Besco_Supermarket
{
    class Receipt
    {
        private string item;

        private string itemDescription;

        private int price;

        public string Item
        {
            get { return item; }
            set { item = value; }
        }

        public string ItemDescription
        {
            get { return itemDescription; }
            set { itemDescription = value; }
        }

        public int Price
        {
            get { return price; }
            set { price = value; }
        }

        public void Display()
        { 
            
        }
    }
}
