﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Besco_Supermarket
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Customer c = new Customer();
        Receipt r = new Receipt();
        private void btnCustomerConfirm_Click(object sender, RoutedEventArgs e)
        {
            c.FirstName = txtBoxFirstName.Text;
            c.LastName = txtBoxLastName.Text;
            c.Address = txtBoxAddress.Text;
            c.Email = txtBoxEmail.Text;
        }

        private void btnOrderConfirm_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                r.Item = cboBoxItem.Text;
                r.Price = int.Parse(lblPrice.Content.ToString());
            }

            catch(Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        private void btnShowReceipt_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Your order is as follows: Customer Name: " + c.FirstName + " " + c.LastName +". Billing and delivery address: " + c.Address
                + ". Email: " + c.Email + ". You ordered: " + r.Item +  ", which is: " + r.ItemDescription +" and costs: £" + r.Price );
        }

        private void cboBoxItem_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cboBoxItem.SelectedIndex == 1)
                lblPrice.Content = 10;
            else if (cboBoxItem.SelectedIndex == 2)
                lblPrice.Content = 5;
            else
                lblPrice.Content = 20;
        }


    }
}
