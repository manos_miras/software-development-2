﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Besco_Supermarket
{
    class Customer
    {
        private string firstName;

        private string lastName;

        private string address;

        private string email;

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public string LastName
        {
            get { return lastName;  }
            set { lastName = value;  }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public void PlaceOrder()
        { 
        
        }

    }
}
