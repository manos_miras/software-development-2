﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    class Person
    {
        private string name;
        private int age;
        private string address;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Age
        {
            get { return age; }
            set { age = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        //public Person()
        //{ 
        //
       //}
        public Person(string nameinput, int ageinput)
        {
            name = nameinput;
            age = ageinput;
        }

        // takes no parameters, returns no value, but prints name, age     and address to the screen
        public void whoami()
        {
            Console.WriteLine("Name: " + name + "\nAge: " + age + "\nAddress: " + address);
        }

        // takes a String parameter and prints it to the screen
        public void say(string parameter)
        {
            Console.WriteLine(parameter);
        }

        // takes no parameters, but adds 1 to age and returns the new value
        public int birthday()
        {
            age = age + 1;
            return age;
        }

    }
}
