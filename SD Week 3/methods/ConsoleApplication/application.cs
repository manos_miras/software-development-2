﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    class application
    {

        static void Main(string[] args)
        {
            // Person p1
            Person p1 = new Person("Matt", 10);
            //p1.Age = 10;
            p1.Address = "High Street";
            //p1.Name = "Matt";

            p1.say("Hello");
            p1.whoami();
            p1.birthday();
            Console.WriteLine("whoami() after birthday()");
            p1.whoami();
            Console.WriteLine();
            // Person p2
            Person p2 = new Person("Bill", 23);
            //p2.Age = 23;
            p2.Address = "Colinton Road";
            //p2.Name = "Bill";

            p2.say("Bye");
            p2.whoami();
            p2.birthday();
            Console.WriteLine("whoami() after birthday()");
            p2.whoami();

            Console.ReadKey();
        }
    }
}
