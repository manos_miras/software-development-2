﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Student_Records_System
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Make a static field of the class
        public static MainWindow Instance;

        public MainWindow()
        {
            // Assign the first window
            if (Instance == null)
                Instance = this;

            InitializeComponent();
        }

        //Checks if a value is numeric or not
        private static Boolean isNumeric(string input)
        {
            //Int output: Used to hold in the output from TryParse
            int output;
            //Checks if entered value is numeric or not
            if (int.TryParse(input, out output) == false)
                return false;
            else
                return true;
        }
        //Checks if a string contains ONLY letters (Also for non-latin alphabets)
        private static Boolean isString(string input)
        {
            //Sets the char that will be the seperator
            char[] delimiterChar = { ' ' };

            //Seperates the given string into seperate array instances
            string[] result = input.Split(delimiterChar);
            
            string noSpace = "";
            //Adds up every instance of the array to noSpace to avoid using spaces (char.IsLetter returns false if it finds a space)
            foreach (string s in result)
                noSpace = noSpace + s;

            return noSpace.All(char.IsLetter);
        }

        private static Boolean hasSpace(string input)
        {
            return input.Contains(" ");
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            //Checks if matriculation and number field are blank, if they are error shows
            if (txtBoxMatriculation.Text == "" || txtBoxMatriculation.Text == " " ||
                txtBoxName.Text == "" || txtBoxName.Text == " ")
                MessageBox.Show("You have left a field blank");
            //Checks if matriculation field is not a number, if not error shows
            else if (isNumeric(txtBoxMatriculation.Text) == false)
                MessageBox.Show("Matriculation number may only contain numbers, please try again.");
            else if (!hasSpace(txtBoxName.Text))
                MessageBox.Show("You must enter a name and a surname at least.");
            //Checks if the input in Name field is a valid name
            else if (isString(txtBoxName.Text) == false)
                    MessageBox.Show("Invalid character in name field, please try again.");
            else
            {
                //Clears all of the textboxes and labels
                txtBoxName.Clear();
                txtBoxMatriculation.Clear();
                lblAddress.Content = "";
                lblCourse.Content = "";

                MessageBox.Show("New student details being added");
            }
        }

        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAddress_Click(object sender, RoutedEventArgs e)
        {
            //Shows the AddressWindow, cannot edit MainWindow until user has closed current window
            var newWindow = new AddressWindow();
            newWindow.ShowDialog();
        }

        private void btnCourse_Click(object sender, RoutedEventArgs e)
        {
            //Show the CourseWindow, cannot edit MainWindow until user has closed current window
            var newWindow = new CourseWindow();
            newWindow.ShowDialog();
        }

        private void btnAbout_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Student Records System: A simple program that records information about University students.\nCreated by Emmanouil Moiras, as part of the SD2 course.\nContact: 40168970@live.napier.ac.uk");
        }
    }
}