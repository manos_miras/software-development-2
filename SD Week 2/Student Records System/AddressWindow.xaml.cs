﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Student_Records_System
{
    /// <summary>
    /// Interaction logic for AddressWindow.xaml
    /// </summary>
    public partial class AddressWindow : Window
    {
        public AddressWindow()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Instance.lblAddress.Content = txtBoxHouseNumber.Text + " " + txtBoxStreet.Text + " "
                                  + txtBoxCity.Text + " " + txtBoxPostcode.Text + " " + txtBoxCountry.Text;
            Close();
        }
    }
}
