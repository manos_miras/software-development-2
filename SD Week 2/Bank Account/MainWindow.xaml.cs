﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Bank_Account
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
 
        // Checks if a value is numeric or not
        private static Boolean isNumeric(string input)
        {
            // Int output: Used to hold in the output from TryParse
            double output;
            // Checks if entered value is numeric or not
            if (double.TryParse(input, out output) == false)
                return false;
            else
                return true;
        }
        // double to keep track of balance
        double balance = 0;
        // Initialize the main window
        public MainWindow()
        {
            InitializeComponent();
            lblBalance.Content = "Your current balance is £" + balance;
        }
        // Adds money to balance when Credit button is pressed
        private void btnCredit_Click(object sender, RoutedEventArgs e)
        {
            // Prevents textbox being empty
            if (txtAmount.Text == " " || txtAmount.Text == "")
                MessageBox.Show("You have not entered an amount.");
            // Prevents textbox having a negative number
            else if (isNumeric(txtAmount.Text) == true && double.Parse(txtAmount.Text) < 0)
                MessageBox.Show("You may not enter a negative number.");
            // Adds money to balance if there is a number in the textbox
            else if (txtAmount.Text.Length > 1 && isNumeric(txtAmount.Text) == true)
            {
                // Cast from string to double
                double amount = double.Parse(txtAmount.Text);
                // Adds amount to balance
                balance = balance + amount;
                // Display balance on appropriate label
                lblBalance.Content = "Your current balance is £" + balance;
                // Adds current transaction to the list log (ListBox)
                lstLog.Items.Add("£" + amount + " deposited, balance = £" + balance);
            }
            // Prevents invalid characters to be entered
            else
            {
                MessageBox.Show("Invalid amount entered");
            }
        }
        // Removes money from balance
        private void btnDebit_Click(object sender, RoutedEventArgs e)
        {
            // Prevents textbox being empty
            if (txtAmount.Text == " " || txtAmount.Text == "")
                MessageBox.Show("You have not entered an amount.");
            // Prevents textbox having a negative number
            else if (isNumeric(txtAmount.Text) == true && double.Parse(txtAmount.Text) < 0)
                MessageBox.Show("You may not enter a negative number.");
            // Removes money from balance if there is a number in the textbox
            else if (txtAmount.Text.Length > 1 && isNumeric(txtAmount.Text) == true)
            {
                // Cast from string to double
                double amount = double.Parse(txtAmount.Text);
                // Prevents transaction from taking place if balance becomes negative
                if (balance - amount < 0)
                {
                    MessageBox.Show("This transaction would result in negative balance. You cannot proceed.");
                }
                else
                {
                    // Removes amount to balance
                    balance = balance - amount;
                    // Display balance on appropriate label
                    lblBalance.Content = "Your current balance is £" + balance;
                    // Adds current transaction to the list log (ListBox)
                    lstLog.Items.Add("£" + amount + " removed, balance = £" + balance);
                }
            }
            // Prevents invalid characters to be entered
            else
            {
                MessageBox.Show("Invalid amount entered");
            }
        }
    }
}
