﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes6
{
    class Program
    {
        static void Main(string[] args)
        {
            Carz c1 = new Carz("somecarmake");
            Carz c2 = new Carz("somecarmake", "somecartype");
            Carz c3 = new Carz("somecarmake", "somecartype", "ultraviolet");

            c1.Display();
            c2.Display();
            c3.Display();


        }
    }
}
