﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes6
{
    class Carz
    {
        private string colour;
        private string type;
        private string make;
        private string country;
        private double cost;

        public Carz() // constructor methods
        {
            country = "UK";
        }
        public Carz(string m)
        {
            country = "UK";
            make = m;
        }
        public Carz(string m, string t) : this(m)
        {
            type = t;
        }

        public Carz(string m, string t, string c) : this(m, t)
        {
            colour = c;
        }

        public string Country // read only Property
        {
            get { return country; }

        }

        public string Colour //property for manipulating colour
        {
            get { return colour; }
            set { colour = value; }
        }
        public string Type //property for manipulating type
        {
            get { return type; }
            set { type = value; }
        }

        public string Make //property for manipulating make
        {
            get { return make; }
            set { make = value; }
        }
        public double Cost //property for manipulating cost
        {
            get { return cost; }
            set { cost = value; }
        }

        public void Display()
        {
            Console.WriteLine
            (
            "This car is a" +
            " " + make +
            " " + type +
            " " + colour
            );
        }
    }
}
