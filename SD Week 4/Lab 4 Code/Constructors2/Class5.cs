using System;

namespace Classes5
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{

			// Put code here to create 3 car instances

            // First car
            AnotherCar car1 = new AnotherCar();// contructor method with no arguments passed
            car1.Colour = "Green";
            car1.Make = "Ford";
            car1.Type = "Mondeo";
            car1.Cost = 18000;


            // Second car
            AnotherCar car2 = new AnotherCar("Ford");// contructor method with no arguments passed
            car2.Colour = "Blue";
            car2.Type = "Mondeo";
            car2.Cost = 17000;

            // Third car
            AnotherCar car3 = new AnotherCar("Ford", "Mondeo");// contructor method with no arguments passed
            car3.Colour = "Pink";
            car3.Type = "Mondeo";
            car3.Cost = 16000;

			// Add a method named 'Display'to the AnotherCar Class
			// This method should output the details of a  car instance
			// to the Console

			// Put code here to call that method to display the 
            car1.Display();
            car2.Display();
            car3.Display();
			// details of a each car instance 
		}
	}
}
