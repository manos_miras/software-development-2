using System;

namespace Classes7
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class7
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			// example of inheritance
			
			Staff p1 = new Staff();
			p1.Name="Fred"; // inherits this from People Class
			p1.ToLowerCase();// inherits this from People Class
			p1.Display();// from Staff Class


            Student p2 = new Student(); // Create new student object
            p2.Name = "Manos";
            p2.MatricNumber = 40168970;
            p2.Display();
            System.Console.ReadLine();
        }
	}
}
