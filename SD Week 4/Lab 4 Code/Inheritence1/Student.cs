﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classes7
{
    class Student : People // Inherits from people
    {
        private int matricNumber;

        public int MatricNumber
        {
            get { return matricNumber; }
            set { matricNumber = value; }
        }

        public void Display()
        {
            Console.WriteLine("Name: " + Name + ", Matric Number: " + matricNumber);
        }
    }
}
