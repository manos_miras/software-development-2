﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1
{
    class Lorry : Vehicle
    {
        private int weight;

        public int Weight
        {
            get { return weight; }
            set { weight = value; }
        }
        public new void Display()
        {
            Vehicle ve = new Vehicle(105012);
            ve.Display();
            Console.WriteLine(" Weight = " + weight);
        }
    }
}
