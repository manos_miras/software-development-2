﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1
{
    public class Car : Vehicle
    {
        private int doors;
        
        public int Doors
        {
            get { return doors; }
            set { doors = value; }

        }

        public new void Display()
        {
            Vehicle ve = new Vehicle(105012);
            ve.Display();
            Console.WriteLine(" Doors = " + doors);
        }
    }
}
