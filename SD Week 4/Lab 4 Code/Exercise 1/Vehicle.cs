﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1
{
    public class Vehicle
    {
        private int regNo;

        public int RegNo
        {
            get { return regNo; }
        }

        public Vehicle()
        {

        }

        public Vehicle(int register)
        {
            regNo = register;
        }

        public void Display()
        {
            Console.Write("Register number: " + regNo);
        }
    }
}
