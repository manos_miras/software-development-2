﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Car test
            Console.WriteLine("Car test");
            Car c = new Car();
            c.Doors = 4;
            c.Display();

            // Bus test
            Console.WriteLine("Bus test");
            Bus b = new Bus();
            b.Seats = 25;
            b.Display();

            // Lorry test
            Console.WriteLine("Lorry test");
            Lorry l = new Lorry();
            l.Weight = 1150;
            l.Display();
        }
    }
}
