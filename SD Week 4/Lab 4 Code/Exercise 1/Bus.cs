﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1
{
    class Bus : Vehicle
    {
        private int seats;

        public int Seats
        {
            get { return seats; }
            set { seats = value; }
        }

        public new void Display()
        {
            Vehicle ve = new Vehicle(105012);
            ve.Display();
            Console.WriteLine(" Seats = " + seats);
        }
    }
}
